import joblib
import numpy as np

# Load the saved model
model = joblib.load('model/LogisticRegression.joblib')
# model = joblib.load('model/RandomForestClassifier.joblib')
# model = joblib.load('model/MLPClassifier.joblib')

# test :
#                           C++         C#          Java        HTML        SQL         Rust
# LogisticRegression        87.83%      85.14%      75.70%      71.86%      99.70%      Unknown
# RandomForestClassifier    75%         65%         59%         51%         84%         Unknown
# MLPClassifier             96.91%      85.51%      74.81%      88.98%      99.59%      Unknown

# Assume 'new_code_snippet' is the new code snippet you want to identify
new_code_snippet = """
// Function that adds two numbers
fn add_numbers(a: i32, b: i32) -> i32 {
    a + b
}

fn main() {
    // Declare variables
    let num1 = 10;
    let num2 = 20;

    // Call the add_numbers function
    let sum = add_numbers(num1, num2);

    // Print the result
    println!("The sum of {} and {} is: {}", num1, num2, sum);

    // Simple conditional statement
    if sum > 30 {
        println!("The sum is greater than 30");
    } else {
        println!("The sum is not greater than 30");
    }
}
"""

# Use the pre-trained model to predict the language of the new code snippet
predicted_probabilities = model.predict_proba([new_code_snippet])[0]

# Set a threshold for confidence in prediction
confidence_threshold = 0.5

# Check if the maximum probability is above the confidence threshold
if np.max(predicted_probabilities) >= confidence_threshold:
    predicted_languages = []

    for i in range(3):
        predicted_language_index = np.argmax(predicted_probabilities)
        predicted_language = model.classes_[predicted_language_index]
        predicted_probability = predicted_probabilities[predicted_language_index]

        predicted_probabilities = np.delete(predicted_probabilities, predicted_language_index)
        predicted_languages.append((predicted_language, predicted_probability))

    print(f"The code is likely written in: {predicted_languages[0][0]} with {predicted_languages[0][1] * 100:.2f}% confidence.\nNext is {predicted_languages[1][0]} with {predicted_languages[1][1] * 100:.2f}% confidence.\nAnd next is {predicted_languages[2][0]} with {predicted_languages[2][1] * 100:.2f}% confidence.")
else:
    print("The model cannot confidently identify the language of the code snippet. It is unknown.")
