import os
import pandas as pd

# Specify your folder path
folder_path = '/Volumes/EMTEC/HectorIA/data'

# Traverse the folder
for filename in os.listdir(folder_path):
    if filename.endswith('.parquet'):
        # Load the Parquet file
        file_path = os.path.join(folder_path, filename)
        df = pd.read_parquet(file_path)
        nb_row = df.shape[0]

        # Divide data
        new_df = df.head(nb_row // 2)
        new_df.to_parquet(folder_path + '/divise/' + filename)

