import joblib
import dask.dataframe as dd
from dask_ml.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_extraction.text import HashingVectorizer
from sklearn.metrics import accuracy_score, classification_report
from sklearn.linear_model import LogisticRegression
from sklearn.neural_network import MLPClassifier
from sklearn.pipeline import Pipeline

# Load the dataset
dataset = dd.read_parquet("data.parquet")
print("data loaded")

# Split the dataset into training and testing sets using Dask
train_df, test_df = train_test_split(dataset, test_size=0.2, random_state=42, shuffle=True)
print("data split")

# Convert Dask DataFrames to Pandas DataFrames
train_df = train_df.compute()
test_df = test_df.compute()
print("data convert")

# Choose a classifier and vectorizer
classifier = LogisticRegression(max_iter=1000, solver='saga')  # 0.91 - 0.87
# classifier = RandomForestClassifier(n_estimators=100, random_state=42) # 0.92 - 0.88
# classifier = MLPClassifier(hidden_layer_sizes=(100,), max_iter=1000) # 0.93 - 0.89

vectorizer = HashingVectorizer(stop_words='english', n_features=2**18)

# Create a pipeline with a HashingVectorizer and a classifier
model = Pipeline([
    ('vectorizer', vectorizer),
    ('classifier', classifier)
])
print("model created")

# Train the model
model.fit(train_df['content'], train_df['lang'])
print("model fit")

# Make predictions on the test set
predictions = model.predict(test_df['content'])

# Evaluate the model
accuracy = accuracy_score(test_df['lang'], predictions)
print(f"Accuracy: {accuracy:.2f}")

# Classification report
print("\nClassification Report:")
print(classification_report(test_df['lang'], predictions))

# Save the trained model to a file
joblib.dump(model, 'model/LogisticRegression.joblib')
print("model save")
