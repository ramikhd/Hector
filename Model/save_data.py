import os
from datasets import load_dataset, concatenate_datasets

# Specify your folder path
folder_path = '/Volumes/EMTEC/HectorIA/data/'

languages = []

# Traverse the folder
for filename in os.listdir(folder_path):
    if filename.endswith('.parquet'):
        # Load the Parquet file
        file_path = os.path.join(folder_path, filename)
        tmp = load_dataset("parquet", data_files={'train': folder_path+filename})
        result = tmp.select_columns(["content", "lang"])
        languages.append(result['train'])

# Concatenate all parquet files
dataset = concatenate_datasets(languages, split='train')
dataset.to_parquet("data.parquet")
