import os
import pandas as pd

# Specify your folder path
folder_path = '/Volumes/EMTEC/HectorIA/data'

# Traverse the folder
for filename in os.listdir(folder_path):
    if filename.endswith('.parquet'):
        # Load the Parquet file
        file_path = os.path.join(folder_path, filename)
        df = pd.read_parquet(file_path)

        # Extract 'lang' attribute and use it for renaming
        lang_value = df['lang'].iloc[0]
        new_filename = f"{lang_value}.parquet"

        # Rename the file
        new_file_path = os.path.join(folder_path, new_filename)
        os.rename(file_path, new_file_path)

        print(f"Renamed {filename} to {new_filename}")
