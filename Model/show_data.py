import dask.dataframe as dd
from dask_ml.model_selection import train_test_split
import matplotlib.pyplot as plt
import joblib
from sklearn.metrics import confusion_matrix
import seaborn as sns
import numpy as np

# Load the dataset
dataset = dd.read_parquet("data.parquet")
train_df, test_df = train_test_split(dataset, test_size=0.2, random_state=42, shuffle=True)

dataset = dataset.compute()

# Group data by language and count the number of code snippets for each language
snippet_counts = dataset.groupby('lang').size()

# Plot the data
plt.bar(snippet_counts.index, snippet_counts.values, color="#4CAF50")
plt.xlabel('Language')
plt.xticks(rotation='vertical')
plt.ylabel('Number of Code Snippets')
plt.title('Code Snippets by Language')
plt.tight_layout()
plt.savefig('diagram_code_by_language.png')
plt.show()

# Charger le modèle sauvegardé
model = joblib.load('model/LogisticRegression.joblib')

true_labels = test_df['lang']
code_snippets = test_df['content']

# Prédire les langages pour chaque extrait de code
predictions = model.predict(code_snippets)

# Créer une matrice de confusion
conf_matrix = confusion_matrix(true_labels, predictions)

# Afficher la matrice de confusion sous forme de nuage de points
plt.figure(figsize=(8, 6))
sns.heatmap(conf_matrix, annot=True, cmap="YlGnBu", xticklabels=model.classes_, yticklabels=model.classes_)
plt.xlabel('Prédiction')
plt.ylabel('Vrai label')
plt.title('Matrice de confusion')
plt.tight_layout()
plt.savefig('matrice_confusion.png')
plt.show()

# Prédire les langages pour chaque extrait de code
predictions = model.predict(code_snippets)

# Compter le nombre d'occurrences de chaque langage
unique_langs, counts = np.unique(predictions, return_counts=True)

# Générer un diagramme circulaire
plt.pie(counts, labels=unique_langs, autopct='%1.1f%%')
plt.title('Répartition des langages prédits')
plt.show()