FROM python:3.8-slim
WORKDIR /App
COPY . /App
RUN pip install --no-cache-dir -r requirements.txt
EXPOSE 443
CMD ["gunicorn", "-b", "0.0.0.0:443", "app:app"]
