# Projet Hector 
[Hector Website](https://codefirst.iut.uca.fr/containers/ramikhedair-hector)

Ce dépôt contient les différents scripts utilisés pour le projet Hector, une IA conçue pour identifier le langage d'un code à partir d'un snippets de code.

## Statistiques de nos données

### Répartition des langages en fonction du nombre d'exemple de code
![diagram_code_by_language.png](Model%2Fdiagram_code_by_language.png)
’
### Matrice de confusion
![matrice_confusion.png](Model%2Fmatrice_confusion.png)

## Démarche du Projet

Le dossier **Model** contient les différents scripts utilisés pour le projet Hector, une IA conçue pour identifier le langage d'un code à partir d'un snippets de code.

### Étape 1 : Renommer les Fichiers

Le script `rename_file.py` est utilisé pour renommer les fichiers Parquet dans le dossier de données en fonction de la valeur de l'attribut **lang** extrait des données. Il utilise la bibliothèque *pandas* pour manipuler les données.

### Étape 2 : Diviser les Données

Le script `divise_data.py` divise les fichiers Parquet puis les enregistre afin de réduire le nombre de ligne du fichier. Il utilise également la bibliothèque *pandas* pour la manipulation des données.

### Étape 3 : Sauvegarder les Données

Le script `save_data.py` charge les fichiers Parquet, extrait les colonnes **content** et **lang**, puis les concatène en un seul jeu de données. Il utilise la bibliothèque *Hugging Face datasets* pour le chargement des données.

### Étape 4 : Entraîner le Modèle

Le script `save_model.py` charge les données à partir du fichier Parquet, divise les données en ensembles d'entraînement et de test, puis entraîne un modèle de classification de langage de code en utilisant différents classificateurs et vecteurs de caractéristiques. Il utilise les bibliothèques *scikit-learn* et *dask* pour le traitement des données et l'entraînement du modèle.

### Étape 5 : Visualiser les Données

Le script `show_data.py` charge les données à partir du fichier Parquet, groupe les données par langage et affiche un diagramme montrant le nombre de snippets de code pour chaque langage. Il utilise les bibliothèques *dask* et *matplotlib* pour la manipulation et la visualisation des données.

### Étape Finale : Utiliser le Modèle

Le script `main.py` charge un modèle pré-entraîné pour identifier le langage de nouveaux snippets de code. Il utilise les bibliothèques *scikit-learn* et *numpy* pour la prédiction et l'analyse des résultats.


## Interface graphique

Le Dossier **App** contient les fichiers nécessaires pour la partie graphique du projet Hector, une application web permettant d'identifier le langage de code à partir de snippets de code.

### Description

L'application utilise Flask, une bibliothèque web légère pour Python, pour créer un serveur web. Elle offre une interface utilisateur simple où les utilisateurs peuvent saisir un fragment de code, et elle renvoie le langage probable de ce code, accompagné d'un niveau de confiance.

### Fonctionnalités

- **Identification du Langage**: L'application utilise un modèle d'apprentissage automatique pré-entraîné pour identifier le langage du code saisi par l'utilisateur.
- **Niveau de Confiance**: La réponse de l'application comprend également un niveau de confiance basé sur les probabilités prédites par le modèle.
- **Interface Web**: L'interface web permet aux utilisateurs d'interagir facilement avec l'application en saisissant leur code et en recevant la réponse instantanément.

### Fichiers Principaux

- **`app.py`**: Le script principal de l'application Flask qui définit les routes et les fonctionnalités de l'application.
- **`MLPClassifier.joblib`**: Le modèle d'apprentissage automatique pré-entraîné utilisé pour prédire le langage du code.
- **`static`**: Ce dossier contient les fichiers statiques tels que le CSS et le JavaScript utilisés pour styliser et dynamiser l'interface web.
- **`templates`**: Ce dossier contient les fichiers HTML utilisés pour générer les pages web de l'application.


## Deploiement Continu

Ce dépôt contient les configurations nécessaires pour le déploiement continu (CD) de l'application Hector. Le déploiement continu garantit que les modifications apportées au code sont automatiquement déployées.

### Description

Le déploiement continu automatise les étapes de construction d'image Docker et de déploiement de conteneurs. Chaque fois qu'un commit est effectué sur la branche principale du dépôt, le pipeline CD est déclenché pour mettre à jour l'application.

### Fichiers Principaux

- **`Dockerfile`**: Le fichier Dockerfile contient les instructions pour construire l'image Docker de l'application. Il spécifie l'environnement de travail, copie les fichiers nécessaires, installe les dépendances et définit la commande par défaut pour exécuter l'application.
- **`.drone.yml`**: Le fichier de configuration pour Drone CD définit les étapes à exécuter lors du déclenchement du pipeline. Il inclut des étapes pour vérifier la syntaxe du Dockerfile, construire et pousser l'image Docker vers un registre, puis déployer le conteneur à l'aide d'un service de proxy Docker.

### Étapes du Pipeline

1. **Vérification du Dockerfile**: Utilisation de Hadolint pour vérifier la syntaxe du Dockerfile et garantir les meilleures pratiques.
2. **Construction et publication de l'image Docker**: Construction de l'image Docker à partir du Dockerfile et publication de l'image sur un registre Docker.
3. **Déploiement du conteneur**: Utilisation d'un service de proxy Docker pour déployer le conteneur à partir de l'image précédemment publiée.

### Secrets

Ce pipeline utilise des secrets pour le nom d'utilisateur et le mot de passe du registre Docker. Assurez-vous de les configurer correctement dans les paramètres de votre pipeline CD.


## Bibliothèques Utilisées

- **Pandas**: Bibliothèque pour la manipulation et l'analyse de données tabulaires en Python, offrant des fonctionnalités puissantes pour filtrer, trier, fusionner et analyser les données.

- **Datasets (Hugging Face)**: Plateforme offrant un accès facile à une grande variété de jeux de données utilisés dans l'apprentissage automatique et le traitement du langage naturel, facilitant ainsi le téléchargement, la gestion et l'utilisation de ces données.

- **Joblib**: Outil pratique pour la sauvegarde et le chargement d'objets Python, particulièrement utile pour conserver les modèles d'apprentissage automatique et les résultats intermédiaires.

- **Dask**: Bibliothèque étendant les capacités de Python pour le traitement parallèle et distribué des données, permettant ainsi de travailler efficacement avec de grands ensembles de données.

- **Scikit-learn**: Bibliothèque complète pour l'apprentissage automatique en Python, proposant une large gamme d'algorithmes et d'outils pour l'entraînement, l'évaluation et le déploiement de modèles.

- **Matplotlib**: Outil de visualisation de données offrant une grande flexibilité pour créer une variété de graphiques et de visualisations permettant d'explorer et de présenter les données de manière efficace.

- **Flask**: Framework web minimaliste pour Python, utilisé pour la création d'applications web grâce à sa simplicité et sa flexibilité.

## Services Utilisés

- **Drone**: Outil d'automatisation de pipeline CI/CD, offrant une manière flexible de définir et d'exécuter des workflows pour les tests, la construction et le déploiement d'applications logicielles.

- **Docker**: Plateforme open-source utilisée pour la création, la gestion et le déploiement de conteneurs logiciels, offrant une manière standardisée de packager des applications et leurs dépendances pour garantir la portabilité et la reproductibilité.

- **Hadolint**: Outil de linting pour Dockerfiles, permettant de vérifier la syntaxe et le style des fichiers Dockerfile afin de détecter les erreurs potentielles et de maintenir des bonnes pratiques de développement.