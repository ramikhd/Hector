import random
import numpy as np
from flask import Flask, render_template, request, jsonify, make_response
import joblib

app = Flask(__name__)
language_prediction_dict = {
    60: {
        "certainty_phrases": [
            "Hmm, pas tout à fait sûr du langage ici.",
            "J'ai du mal à identifier exactement le langage.",
            "Le langage du code est un peu insaisissable pour le moment.",
            "C'est un peu difficile d'identifier le langage dans ce fragment de code.",
            "Le langage ici est un puzzle que je suis toujours en train de résoudre.",
            "Je suis incertain du langage de ce fragment de code.",
            "L'identification du langage est un défi.",
            "Le langage du code semble incertaine à ce niveau de confiance.",
            "Je ne suis pas confiant pour identifier le langage ici.",
            "C'est un peu un mystère ; je ne suis pas sûr du langage du code."
        ]
    },
    70: {
        "certainty_phrases": [
            "Ça pourrait être du [Langage], mais je n'en suis pas tout à fait sûr. ",
            "Il y a un léger soupçon de [Langage], mais ce n'est pas définitif. ",
            "Je penche pour du [Language], mais avec une certaine incertitude. ",
            "C'est peut-être écrit en [Langage], mais je ne peux pas en être certain. ",
            "Il y a une possibilité que ce code soit en [Langage], mais ce n'est pas confirmé. ",
            "Je détecte des caractéristiques du [Langage], mais ce n'est pas concluant. ",
            "[Langage] semble familier, mais ce n'est pas clair dans ce code. ",
            "Cela pourrait être en [Langage], mais je ne suis pas entièrement convaincu. ",
            "Je remarque certaines caractéristiques de [Langage], mais ce n'est pas définitif. ",
            "Il y a une probabilité que ce soit en [Langage], mais avec incertitude. "
        ],
    },
    80: {
        "certainty_phrases": [
            "Je suis assez confiant que cela soit écrit en [Langage].",
            "Le code ressemble fortement à du [Langage].",
            "Ceci semble être identifié avec confiance comme étant du [Langage].",
            "Il y a une forte probabilité que ce code soit en [Langage].",
            "Je suis assez sûr du [Langage] dans ce fragment de code.",
            "Les caractéristiques pointent fortement vers [Langage].",
            "Identifier cela avec confiance avec un haut degré de certitude en [Langage].",
            "Le code présente des signes clairs d'appartenance à [Langage].",
            "Je suis confiant dans mon identification de [Langage] dans ce fragment de code.",
            "Les caractéristiques indiquent fortement que c'est écrit en [Langage]."
        ],
    },
    90: {
        "certainty_phrases": [
            "Je suis très confiant que cela soit écrit en [Langage].",
            "Les preuves soutiennent fortement l'identification de [Langage].",
            "Confiance élevée dans l'identification du code comme étant [Langage].",
            "C'est indéniablement écrit en [Langage].",
            "Les caractéristiques du code ne laissent aucun doute sur l'identification de [Langage].",
            "J'ai un niveau élevé de confiance dans l'identification de [Langage].",
            "C'est clair ; ce code est en [Langage] sans aucun doute.",
            "Les caractéristiques suggèrent fortement [Langage] dans ce code.",
            "Il y a des preuves solides indiquant [Langage] dans ce code.",
            "Je suis hautement confiant que ce code soit en [Langage]."
        ],

    },
    91: {
        "certainty_phrases": [
            "Je suis presque certain que cela soit écrit en [Langage].",
            "Les preuves pointent de manière écrasante vers [Langage].",
            "Il est sûr de dire que ce code est en [Langage] sans aucun doute.",
            "C'est aussi certain que possible ; le code est en [Langage].",
            "Les caractéristiques ne laissent aucune place au doute ; c'est en [Langage].",
            "Je suis extrêmement confiant dans l'identification de [Langage] dans ce code.",
            "Les preuves écrasantes soutiennent la conclusion que c'est en [Langage].",
            "Il n'y a aucune place pour l'incertitude ; c'est clairement en [Langage].",
            "Je peux affirmer avec une quasi-certitude que ce code est en [Langage].",
            "C'est très clair ; le code est majoritairement en [Langage]."
        ],
    }
}


def get_completion(prompt):
    model = joblib.load('MLPClassifier.joblib')
    predicted_probabilities = model.predict_proba([prompt])[0]

    predicted_languages = []

    for i in range(3):
        predicted_language_index = np.argmax(predicted_probabilities)
        predicted_language = model.classes_[predicted_language_index]
        predicted_probability = predicted_probabilities[predicted_language_index] * 100

        predicted_probabilities = np.delete(predicted_probabilities, predicted_language_index)
        predicted_languages.append((predicted_language, predicted_probability))

    if predicted_languages[0][1] < 60:
        answer = random.choice(language_prediction_dict[60]["certainty_phrases"])
    else:
        for level, phrases in sorted(language_prediction_dict.items()):
            if predicted_languages[0][1] > level:
                answer = random.choice(phrases["certainty_phrases"]).replace("[Langage]", predicted_languages[0][0])
    return answer + f"\n({predicted_languages[0][0]}: {predicted_languages[0][1]:.2f}%, {predicted_languages[1][0]}: {predicted_languages[1][1]:.2f}%, {predicted_languages[2][0]}: {predicted_languages[2][1]:.2f}%)"

@app.route("/", methods=['POST', 'GET'])
def query_view():
    if request.method == 'POST':
        prompt = request.form['prompt']
        response = get_completion(prompt)
        return make_response(jsonify({'response': response}), 200)

    css_file_path = 'static/index.css'
    js_file_path = 'static/index.js'
    with open(css_file_path, 'r') as css_file:
        css_content = css_file.read()
    with open(js_file_path, 'r') as css_file:
        js_content = css_file.read()
    return render_template('index.html', css_content=css_content, js_content=js_content)


if __name__ == "__main__":
    app.run(debug=True)