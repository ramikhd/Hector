function appendMessage(name, img, side, text) {
    // Simple solution for small apps
    var dateTime = new Date();
    var time = dateTime.toLocaleTimeString();
    const msgHTML = `
        <div class="msg ${side}-msg">
            <div class="msg-img" style="background-image: url(${img})"></div>
            <div class="msg-bubble">
                <div class="msg-info">
                    <div class="msg-info-name">${name}</div>
                    <div class="msg-info-time">${time}</div>
                </div>
                    <p class="msg-text" style="max-width: 450px;${side === 'right' ? 'white-space: pre-wrap; overflow-wrap: break-word;' : ''}"">
                        ${escapeHtml(text)}
                    </p>
            </div>
        </div>
    `;

    // Append the message HTML to the chat window
    $("#response").append(msgHTML);
    var container = $("#response");
    container.scrollTop(container.prop("scrollHeight"));
}
function escapeHtml(text) {
    return text.replace(/</g, '&lt;').replace(/>/g, '&gt;');

}
$(document).ready(function () {

    // Send the form on enter keypress and avoid if shift is pressed
    $('#prompt').keypress(function (event) {
        if (event.keyCode === 13 && !event.shiftKey) {
            event.preventDefault();
            $('form').submit();
        }
    });

    $('form').on('submit', function (event) {
        event.preventDefault();
        if ( $('#prompt').val().trim() === ""){
            return;
        }
        // Get the CSRF token from the cookie
        var csrftoken = Cookies.get('csrftoken');

        // set the CSRF token in the AJAX headers
        $.ajaxSetup({
            headers: { 'X-CSRFToken': csrftoken }
        });

        // Get the prompt
        var prompt = $('#prompt').val();
        var dateTime = new Date();
        var time = dateTime.toLocaleTimeString();

        // Add the prompt to the response div
        appendMessage("Me", "https://xsgames.co/randomusers/avatar.php?g=pixel", "right", prompt);
        // Clear the prompt
        $('#prompt').val('');

        $.ajax({
            url: '/containers/ramikhedair-hector',
            type: 'POST',
            data: { prompt: prompt },
            dataType: 'json',
            success: function (data) {
                appendMessage("Hector", "https://media.discordapp.net/attachments/1151459295037239367/1153703413943648266/logo2.png?ex=65cb8088&is=65b90b88&hm=d2bcfbc3423c884e3f86d94eb0d0f69c25bc72366bca23de5296ac24a52539b6&=&format=webp&quality=lossless&width=461&height=461", "left", data.response);
            },
            error: function (xhr, status, error) {
            console.error('xhr Error:', xhr);
                console.error('AJAX Error:', status, error);
            }
        });
    });
});
